# Task

This repo contains two tasks in separate folders. Please fork the project, add
your solution for each task and make a pull request. You may either leave the
original code intact and add your solution in separate directories or completely
rewrite everything.

Comments explaining your code, decisions and any relevant thoughts or concerns
are welcome.

Thank you!
